Setup
===============

1. Install Java8, NPM, Python 2.7

2. Run `./gradlew setup` (Can fail for web-client in Windows)

3. Install Redis. Create new db `react-wiki`

It works for linux and Mac. But can have a problems with Windows.


Web Client Dev Server
===============

To start the server try `npm start` in `web-client` folder

Web Server
===============

Run server:

    ./gradlew :core:run

Optional environment variables:

    * DATABASE_URL [default: jdbc:postgresql://localhost/react-wiki]
    
    * DATABASE_USER [default: postgres]
    
    * DATABASE_PASSWORD [default: <empty>]
  
Examples:

  * run server and specify database password
  
        `DATABASE_PASSWORD=root ./gradlew :core:run`
