require('es6-promise').polyfill();
var path = require('path');

module.exports = {
    entry: "./src/app.js",
    output: {
        path: path.join(__dirname, "build", "output"),
        filename: "app.js"
    },
    module: {
        loaders: [
            {
              test: /\.css$/,
              loader: "style!css"
            },
            {
              test: /\.(js|jsx)$/,
              include: [
                path.resolve(__dirname, "src")
              ],
              loader: "babel"
            },
            {
              test: /\.(ttf|eot|svg|woff(2)?)$/,
              loader: "file"
            }
        ]
    }
};
