import Reflux from 'reflux'
import React from 'react'

import Actions from './page_actions'
import HomePage from '../views/pages/home'

var currentPage = <HomePage />;

export default Reflux.createStore({
    init: function() {
        this.listenTo(Actions.change, this.updatePage);
    },

    currentPage: function() {
        return currentPage;
    },

    updatePage: function(page) {
        currentPage = page;
        this.trigger(page);
    }
});