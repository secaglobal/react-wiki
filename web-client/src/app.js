require('bootstrap/dist/css/bootstrap.css');

import  _  from 'lodash'
import app from 'ampersand-app'
import Router from './router'
import domReady from 'domready'
import React from 'react'
import ReactDOM from 'react-dom'

import Layout from './views/layouts/layout.js'

// attach our app to `window` so we can
// easily access it from the console.
window.app = app;

app.extend({
    router: new Router(),

    init: function() {
        this.router.history.start({ pushState: false });
        ReactDOM.render(<Layout />, document.getElementById('root'));
    },

    navigate: function(page) {
        var url = (page.charAt(0) === '/') ? page.slice(1) : page;
        this.router.history.navigate(url, {
            trigger: true
        });
    }
});

// run it on domReady
domReady(_.bind(app.init, app));
