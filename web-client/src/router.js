var app = require('ampersand-app');
var Router = require('ampersand-router');
var React = require('react');

import PageActions from './page/page_actions'
import LoginPage from './views/pages/login'
import HomePage from './views/pages/home'


module.exports = Router.extend({
    routes: {
        '': 'home',
        'login': 'login',
        '(*path)': 'catchAll'
    },

    home: function () {
        PageActions.change( <HomePage /> );
    },

    login: function () {
        PageActions.change( <LoginPage /> );
    },

    catchAll: function () {
        this.redirectTo('');
    }
});
