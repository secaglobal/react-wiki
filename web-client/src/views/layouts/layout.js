import React from 'react';
import PageStore from './../../page/page_store'

export default class extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            page: PageStore.currentPage()
        };

        PageStore.listen(this.onRouteChange.bind(this));
    }

    onRouteChange(page) {
        this.setState({page: page});
    }

    render() {
        return <div>
            <nav className="navbar navbar-default">
                <div className="container-fluid">
                    <div className="navbar-header">
                        <a className="navbar-brand" href="#">react-wiki</a>
                    </div>
                    <ul className="nav navbar-nav">
                        <li><a href="#">home</a></li>
                        <li><a href="#login">login</a></li>
                    </ul>
                </div>
            </nav>
            <main id="root">{this.state.page}</main>
        </div>
    }
}
