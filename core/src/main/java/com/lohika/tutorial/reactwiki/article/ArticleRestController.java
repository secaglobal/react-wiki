package com.lohika.tutorial.reactwiki.article;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * Article rest controller
 * @since 2015-11-10
 * @author Sergey Levandovskiy <slevandovskiy@lohika.com>
 */
@Controller
public class ArticleRestController {
    @Autowired
    public ArticleRepository articleRepository;

    @RequestMapping(value = "/articles", method = RequestMethod.GET)
    public @ResponseBody Iterable<? extends Article> getArticles() {
        return articleRepository.findAll();
    }

    @RequestMapping(value = "/article/{id}", method = RequestMethod.GET)
    public @ResponseBody Article getArticle(@PathVariable("id") Long id) {
        return articleRepository.findOne(id);
    }

    @RequestMapping(value = "/article", method = RequestMethod.POST)
    public @ResponseBody Article saveArticle(@RequestBody ArticleImpl article) {
        return articleRepository.save(article);
    }

    @RequestMapping(value = "/article/{id}", method = RequestMethod.DELETE)
    public @ResponseBody void deleteArticle(@PathVariable("id") Long id) {
        articleRepository.delete(id);
    }
}
