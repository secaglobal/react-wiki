package com.lohika.tutorial.reactwiki.article;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * This is a main content of a react-wiki
 * @since 2015-11-10
 * @author Sergey Levandovskiy <slevandovskiy@lohika.com>
 */
@Repository
public interface ArticleRepository extends CrudRepository<ArticleImpl, Long> {
}
