package com.lohika.tutorial.reactwiki.common;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Base interface for data entities
 * @since 2015-11-11
 * @author Sergey Levandovskiy <slevandovskiy@lohika.com>
 */
@JsonIgnoreProperties("new")
public interface BaseEntity {
}
