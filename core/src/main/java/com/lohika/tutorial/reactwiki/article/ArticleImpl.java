package com.lohika.tutorial.reactwiki.article;

import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Base {@code Article} implementation
 * @since 2015-11-10
 * @author Sergey Levandovskiy <slevandovskiy@lohika.com>
 */
@Entity
@Table(name = "Article")
public class ArticleImpl extends AbstractPersistable<Long> implements Article {
    private String title;

    private String text;

    /**
     * @see Article#getTitle()
     */
    @Override
    public String getTitle() {
        return title;
    }


    /**
     * @see Article#setTitle(String)
     */
    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @see Article#getText()
     */
    @Override
    public String getText() {
        return text;
    }

    /**
     * @see Article#setText(String)
     */
    @Override
    public void setText(String text) {
        this.text = text;
    }
}
