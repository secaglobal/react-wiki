package com.lohika.tutorial.reactwiki;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

/**
 * Application entry point
 * @since 2015-11-10
 * @author Sergey Levandovskiy <slevandovskiy@lohika.com>
 */
@EnableAutoConfiguration
@ComponentScan
public class Application {
    public static void main(String[] args) throws Exception {
        SpringApplication.run(Application.class, args);
    }
}
