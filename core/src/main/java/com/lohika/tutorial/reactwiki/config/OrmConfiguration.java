package com.lohika.tutorial.reactwiki.config;

import com.typesafe.config.Config;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

/**
 * Spring ORM configuration
 * @since 2015-11-10
 * @author Sergey Levandovskiy <slevandovskiy@lohika.com>
 */
@Configuration
@EnableJpaRepositories("com.lohika.tutorial.reactwiki")
@EnableTransactionManagement
public class OrmConfiguration {

    @Bean
    public EntityManagerFactory getEntityManagerFactory(DataSource dataSource) {
        Properties properties = new Properties();
        properties.setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQL94Dialect");

        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        vendorAdapter.setGenerateDdl(true);

        LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
        factory.setJpaVendorAdapter(vendorAdapter);
        factory.setDataSource(dataSource);
        factory.setPackagesToScan("com.lohika.tutorial.reactwiki");
        factory.setJpaProperties(properties);
        factory.afterPropertiesSet();
        return factory.getObject();
    }

    @Bean(destroyMethod = "close")
    public DataSource getDataSource(Config config) {
        Properties dbProperties = new Properties();
        dbProperties.setProperty("url", config.getString("db.url"));
        dbProperties.setProperty("user", config.getString("db.user"));
        dbProperties.setProperty("password", config.getString("db.password"));

        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setPoolName("default");
        dataSource.setConnectionTestQuery("SELECT 1");
        dataSource.setDataSourceClassName("org.postgresql.jdbc2.optional.SimpleDataSource");
        dataSource.setDataSourceProperties(dbProperties);

        return dataSource;
    }
}
