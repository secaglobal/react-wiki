package com.lohika.tutorial.reactwiki.article;

import com.lohika.tutorial.reactwiki.common.BaseEntity;

/**
 * This is a main content of a react-wiki
 * @since 2015-11-10
 * @author Sergey Levandovskiy <slevandovskiy@lohika.com>
 */
interface Article extends BaseEntity {
    /**
     * Returns title of the article
     * @return title of the article
     */
    String getTitle();

    /**
     * Sets a title of the article
     * @param title Title of the article
     */
    void setTitle(String title);

    /**
     * Returns description of the article
     * @return description of the article
     */
    String getText();

    /**
     * Sets a text of the article
     * @param text Text of the article
     */
    void setText(String text);
}
