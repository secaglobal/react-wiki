package com.lohika.tutorial.reactwiki.config;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Global application configuration
 * @since 2015-11-11
 * @author Sergey Levandovskiy <slevandovskiy@lohika.com>
 */
@Configuration
public class ApplicationConfiguration {

    @Bean
    public Config getConfig() {
        return ConfigFactory.load();
    }
}
